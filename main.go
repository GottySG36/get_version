// Simple utility to increment programmatically the version number of a program

package main

import (
    "flag"
    "fmt"
    "log"
    "os/exec"
    "regexp"
    "strconv"
    "strings"
)

type Version struct {
    Release int
    Minor   int
    Fix     int
}

var version = flag.String("v", "", "Current version to increment. Required format : 'vX.Y.Z'")
var specific = flag.String("use-version", "", "Specify a specific version to use as tag. This version is final and will not be incremented.")
var release = flag.Bool("release", false, "Will increment 'Release' level and reset numbering.")
var minor = flag.Bool("minor", false, "Will increment 'Minor' level and reset numbering.")
var fix = flag.Bool("fix", false, "Will increment 'Fix' level.")

var git = flag.Bool("git", false, "Tells the script that you are working in a git repo and will automatically add a new tag.")
var prt = flag.Bool("print", false, "Will only printthe current version. Useful to get the current version from git and not actually doing anything.")

var reg_v = regexp.MustCompile(`^v(\d+|(\d+\.){1,2}\d+)$`)

func ValidateFlags(f ...bool) (int, error) {
    var tot int
    for _, b := range f {
        if b { tot++ }
    }
    if tot > 1 {
        return tot, fmt.Errorf("MultipleFlags : multiple flags specified (%v), only use one at a time.", tot)
    }
    return tot, nil
}

func ValidateVersion(v string) bool {
    return reg_v.MatchString(v)
}

func GetVersion(v string) *Version {
    vers := new(Version)
    ints := strings.Split(v[1:], ".")
    switch len(ints) {
    case 3  :
        r, err := strconv.Atoi(ints[0])
        if err != nil {
            log.Fatalf("Error -:- SplitVersion : %v\n", err)
        }
        m, err := strconv.Atoi(ints[1])
        if err != nil {
            log.Fatalf("Error -:- SplitVersion : %v\n", err)
        }
        f, err := strconv.Atoi(ints[2])
        if err != nil {
            log.Fatalf("Error -:- SplitVersion : %v\n", err)
        }
        vers.Release = r
        vers.Minor = m
        vers.Fix = f

    case 2  :
        r, err := strconv.Atoi(ints[0])
        if err != nil {
            log.Fatalf("Error -:- SplitVersion : %v\n", err)
        }
        m, err := strconv.Atoi(ints[1])
        if err != nil {
            log.Fatalf("Error -:- SplitVersion : %v\n", err)
        }
        vers.Release = r
        vers.Minor = m

    case 1  :
        r, err := strconv.Atoi(ints[0])
        if err != nil {
            log.Fatalf("Error -:- SplitVersion : %v\n", err)
        }
        vers.Release = r

    default :
        log.Fatalf("Error -:- SplitVersion : An unknown error occured, could not find out the number of items in version %v\n", v)
    }

    return vers
}

func (v *Version) IncRelease() {
    v.Release++
    v.Minor = 0
    v.Fix = 0
}

func (v *Version) IncMinor() {
    v.Minor++
    v.Fix = 0
}

func (v *Version) IncFix() {
    v.Fix++
}

func (v *Version) String() string {
    return fmt.Sprintf("v%v.%v.%v", v.Release, v.Minor, v.Fix)
}

func GitGetVersionString() (string, error) {
    out, err := exec.Command("git", "describe", "--tags", "--abbrev=0").Output()
    if err != nil {
        return "", err
    }
    return strings.TrimSuffix(string(out), "\n"), nil
}

func (v *Version) GitUpdateVersion() error {

    _, err := exec.Command("git", "tag", fmt.Sprint(v)).Output()
    if err != nil {
        return err
    }
    _, err = exec.Command("git", "push", "origin", fmt.Sprint(v)).Output()
    if err != nil {
        return err
    }

    return nil
}

func main() {
    flag.Parse()
    _, err := ValidateFlags(*release, *minor, *fix)
    if err != nil {
        log.Fatalf("Error -:- ValidateFlags : %v\n", err)
    }

    var v *Version
    switch {
    case *specific != "" :
        if !ValidateVersion(*specific) {
            log.Fatalf("Error -:- ValidateVersion : Invalid specific version provided (%v). Use 'vX.Y.Z', or a subset of it, numbers only", *specific)
        }
        v = GetVersion(*specific)

    case *version != "" :
        if !ValidateVersion(*version) {
            log.Fatalf("Error -:- ValidateVersion : Invalid version provided (%v). Use 'vX.Y.Z', or a subset of it, numbers only", *version)
        }
        v = GetVersion(*version)
    case *git :
        vGit, err := GitGetVersionString()
        if err != nil {
            log.Fatalf("Error -:- GitGetVersionString : %v\n", err)
        }

        if !ValidateVersion(vGit) {
            log.Fatalf("Error -:- ValidateVersion : Invalid or not supported version obtained from git (%v). Should be 'vX.Y.Z', or a subset of it, numbers only", vGit)
        }
        v = GetVersion(vGit)

    }

    switch {
    case *specific != "":
        log.Printf("Info -:- A specific version has been specified. We will use it to proceed.\n")

    case *release:
        v.IncRelease()

    case *minor:
        v.IncMinor()

    case *fix:
        v.IncFix()
    case *prt:
        log.Printf("Info -:- Since no flags were specified, we simply print the current version.\n")
        fmt.Printf("Current version is... ")

    default:
        log.Fatalf("Error -:- Since no flags were specified, we could not know which level to increment.")
    }

    switch {
    case *prt:
        fmt.Printf("Version : %v\n", v)
    case *git:
        fmt.Printf("Using git... New version : %v\n", v)
        err = v.GitUpdateVersion()
        if err != nil {
            log.Fatalf("Error -:- GitUpdateVersion : %v\n", err)
        }
        fmt.Printf("Successfully updated to tag %v\n", v)

    default:
        fmt.Printf("New version : %v\n", v)
    }
}


